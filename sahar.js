import React, {Component, Fragment} from 'react';
import SignUpForm from 'screens/signUp/signupForm';
import ConfirmForm from 'screens/signUp/confirmForm'
import * as constants from './constants';
import { Content } from 'native-base';
import Header from 'components/header'
import translate from "i18n";

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: null,
            viewStatus: constants.NUMBER_VIEW,
        };

    }
    formData = FormData => {
        this.setState({
            formData: FormData
        })
    };
    viewStatusCallback = viewStatus => {
        this.setState({
            viewStatus: viewStatus
        })
    };
    renderFarm = () => {
        const { viewStatus } = this.state;
        if (viewStatus === constants.NUMBER_VIEW) {
            return (
                <SignUpForm formDataCallBack={this.formData} viewStatusCallback={this.viewStatusCallback} />
            )
        } else if (viewStatus === constants.CONFIRM_VIEW) {
            return (
                <ConfirmForm formData={this.state.formData} viewStatusCallback={this.viewStatusCallback} navigation={this.props.navigation} />
            )
        }
    };
    render() {
        return (
            <Fragment>
                <Header title={translate('signUpButtonPage')}/>
                <Content showsVerticalScrollIndicator={false}>
                    {this.renderFarm()}
                </Content>
            </Fragment>
        )
    }
}
