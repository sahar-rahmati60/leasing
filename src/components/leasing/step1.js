import React,{Component} from 'react';

class Step1 extends Component {
    continue = e => {
      e.preventDefault();
      this.props.nextStep();
    }

    render() {
      const {values , handleChange} = this.props;

        return (
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <form>
                    <div className="form-group">
                      <label>نام</label>
                      <input
                        className="form-control"
                        onChange={handleChange('firstName')}
                        defaultValue={values.firstName}
                        />
                      </div>
                      <div className="form-group">
                        <label>Last Name</label>
                        <input
                          className="form-control"
                          onChange={handleChange('lastName')}
                          defaultValue={values.lastName}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Email</label>
                        <input
                          className="form-control"
                          onChange={handleChange('email')}
                          defaultValue={values.email}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Father Name</label>
                        <input
                          className="form-control"
                          onChange={handleChange('fatherName')}
                          defaultValue={values.fatherName}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Birthday</label>
                        <input
                          className="form-control"
                          onChange={handleChange('birthday')}
                          defaultValue={values.birthday}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Marriage Status</label>
                        <input
                          className="form-control"
                          onChange={handleChange('marriageStatus')}
                          defaultValue={values.marriageStatus}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>National Code</label>
                        <input
                          className="form-control"
                          onChange={handleChange('nationalCode')}
                          defaultValue={values.nationalCode}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Id Number</label>
                        <input
                          className="form-control"
                          onChange={handleChange('idNumber')}
                          defaultValue={values.idNumber}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Mobile</label>
                        <input
                          className="form-control"
                          onChange={handleChange('mobile')}
                          defaultValue={values.mobile}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Adress</label>
                        <input
                          className="form-control"
                          onChange={handleChange('address')}
                          defaultValue={values.address}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>phone</label>
                        <input
                          className="form-control"
                          onChange={handleChange('phone')}
                          defaultValue={values.phone}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Company</label>
                        <input
                          className="form-control"
                          onChange={handleChange('company')}
                          defaultValue={values.company}
                          />
                      </div>
                      <button onClick={this.continue} className="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default Step1
