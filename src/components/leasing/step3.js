import React,{Component} from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';


const POST_MUTATION = gql`
  mutation CreateRequest(
    $firstName: String!,
    $lastName : String!,
    $fatherName: String!,
    $birthday : String!,
    $marriageStatus : String!,
    $nationalCode: String!,
    $idNumber : String!,
    $mobile: String!,
    $address:String!,
    $phone:String!,
    $email: String!,
    $company:String!,
    $jobTitle:String!,
    $jobAddress: String!,
    $jobPhone:String!,
    $bankAccount:String!,
    $bank:String!,
    $bankAccountCreationDate:String!,
    $bankBranch:String!,
    $products:String!,
    $total:String!,
    $prepayment:String!,
    $checksNumber:String!) {
    createFinanceRequest(
      firstName: $firstName,
      lastName : $lastName,
      fatherName:$fatherName,
      birthday :$birthday,
      marriageStatus :$marriageStatus,
      nationalCode:$nationalCode,
      idNumber :$idNumber,
      mobile:$mobile,
      address:$address,
      phone:$phone,
      email:$email,
      company:$company,
      jobTitle:$jobTitle,
      jobAddress:$jobAddress,
      jobPhone:$jobPhone,
      bankAccount:$bankAccount,
      bank:$bank,
      bankAccountCreationDate:$bankAccountCreationDate,
      bankBranch:$bankBranch,
      products:$products,
      total:$total,
      prepayment:$prepayment,
      checksNumber:$checksNumber) {
      id
      firstName
      lastName
      fatherName
      birthday
      marriageStatus
      nationalCode
      idNumber
      mobile
      address
      phone
      email
      company
      jobTitle
      jobAddress
      jobPhone
      bankAccount
      bank
      bankAccountCreationDate
      bankBranch
      products
      total
      prepayment
      checksNumber
    }
  }
`

class Step3 extends Component {
    continue = e => {
      e.preventDefault();
      this.props.nextStep();
    }

    back = e => {
      e.preventDefault();
      this.props.prevStep();
    }
    render() {
      const {values:{firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
        jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber}} = this.props;

        return (
            <div className="container">
              <div className="row">
                <div className="col-12">
                    <ul>
                      <li>firs tName: {firstName}</li>
                      <li>last Name : {lastName}</li>
                      <li>father Name : {fatherName}</li>
                      <li>birthday : {birthday}</li>
                      <li>marriage Status :{marriageStatus}</li>
                      <li>national Code : {nationalCode}</li>
                      <li>idNumber : {idNumber}</li>
                      <li>mobile : {mobile}</li>
                      <li>address : {address}</li>
                      <li>phone : {phone}</li>
                      <li>email : {email} </li>
                      <li>company : {company}</li>
                      <li>jobTitle : {jobTitle}</li>
                      <li>jobAddress : {jobAddress} </li>
                      <li>jobPhone : {jobPhone}</li>
                      <li>bankAccount : {bankAccount}</li>
                      <li>bank : {bank}</li>
                      <li>bankAccountCreationDate : {bankAccountCreationDate}</li>
                      <li>bankBranch : {bankBranch}</li>
                      <li>products : {products}</li>
                      <li>total : {total}</li>
                      <li>prepayment : {prepayment}</li>
                      <li>checksNumber : {checksNumber}</li>
                    </ul>
                    <Mutation mutation={POST_MUTATION}  variables={{firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
                      jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber}}>
                      {createFinanceRequest => <button onClick={createFinanceRequest}>Submit</button>}
                    </Mutation>

                    <button onClick={this.back} className="btn btn-secondary ">Bck</button>
                </div>
              </div>
            </div>
        );
    }
}

export default Step3
