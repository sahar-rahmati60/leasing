import React,{Component} from 'react';

class Actions extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  }

    render() {
        return (
            <button onClick={this.continue} >
              Next Step
            </button>
        );
    }
}

export default Actions
