import React,{Component} from 'react';
import {Nav, NavItem, NavLink} from 'reactstrap'

import styles from './style/leasing.scss'

class Navigation extends Component {

    render() {
        return (
          <Nav className={styles.Nav}>
            <NavItem className={styles.NavItem}>
              <NavLink className={`${styles.NavLink} ${styles.Citcle}`} href="#">Link</NavLink>
            </NavItem>
            <NavItem className={styles.NavItem}>
              <NavLink className={styles.NavLink} href="#">Link</NavLink>
            </NavItem>
            <NavItem className={styles.NavItem}>
              <NavLink className={styles.NavLink} href="#">Another Link</NavLink>
            </NavItem>
            <NavItem className={styles.NavItem}>
              <NavLink className={styles.NavLink} href="#">Disabled Link</NavLink>
            </NavItem>
          </Nav>
        );
    }
}

export default Navigation
