import React,{Component} from 'react'
import {Container, Row, Col} from 'reactstrap'

import styles from './style/leasing.scss'

import Leasing from './leasing'
import Navigation from './navigation'
import Actions from './actions'

class Main extends Component {

    render() {
      return(
        <Container className={styles.GridContainer}>
          <Row className={styles.GridRow}>
            <Col className={styles.Grid12}>
              <Navigation />
              <Leasing />
              <Actions />
            </Col>
          </Row>
        </Container>
      )
    }
}

export default Main
