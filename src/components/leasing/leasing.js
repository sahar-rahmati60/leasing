import React,{Component} from 'react';

import Step1 from './step1';
import Step2 from './step2';
import Step3 from './step3'



class Leasing extends Component {

    state = {
      step: 1 ,
      firstName : '',
      lastName : '',
      fatherName: '',
      birthday : '',
      marriageStatus : '',
      nationalCode: '',
      idNumber : '',
      mobile: '',
      address:'',
      phone:'',
      email: '',
      company:'',
      jobTitle:'',
      jobAddress: '',
      jobPhone:'',
      bankAccount:'',
      bank:'',
      bankAccountCreationDate:'',
      bankBranch:'',
      products:'',
      total:'',
      prepayment:'',
      checksNumber:''
    }


// go to next Level
nextStep = () => {
 const { step } = this.state;
 this.setState({
     step : step + 1
 });
}

// go back previus level
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step : step - 1
        });
    }

// handel Feild change
    handleChange = input => event => {
         this.setState({ [input] : event.target.value })
     }
    render() {
      const {step} = this.state;
      const {firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
        jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber} = this.state;
      const values = {firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
        jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber}
      switch (step) {
          case 1 :
          return (
            <Step1
              nextStep = {this.nextStep}
              handleChange = {this.handleChange}
              values = {values}
            />
          )
          case 2 :
          return (
            <Step2
              nextStep = {this.nextStep}
              handleChange = {this.handleChange}
              prevStep = {this.prevStep}
              values = {values}
            />
          )
          case 3 :
          return (
            <Step3
              nextStep = {this.nextStep}
              handleChange = {this.handleChange}
              prevStep = {this.prevStep}
              values = {values}
            />
          )
          default:
              return <h1>STEP 4</h1>

      }
    }
}

export default Leasing
