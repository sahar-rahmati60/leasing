import React,{Component} from 'react';



class Step2 extends Component {
    continue = e => {
      e.preventDefault();
      this.props.nextStep();
    }
    back = e => {
      e.preventDefault();
      this.props.prevStep();
    }
    render() {
      const {values , handleChange} = this.props;
        return (
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <form>
                    <div className="form-group">
                      <label>Job Title</label>
                      <input
                        className="form-control"
                        onChange={handleChange('jobTitle')}
                        defaultValue={values.jobTitle}
                        />
                      </div>
                      <div className="form-group">
                        <label>Job Address</label>
                        <input
                          className="form-control"
                          onChange={handleChange('jobAddress')}
                          defaultValue={values.jobAddress}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Job Phone</label>
                        <input
                          className="form-control"
                          onChange={handleChange('jobPhone')}
                          defaultValue={values.jobPhone}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Bank</label>
                        <input
                          className="form-control"
                          onChange={handleChange('bank')}
                          defaultValue={values.bank}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Bank Account</label>
                        <input
                          className="form-control"
                          onChange={handleChange('bankAccount')}
                          defaultValue={values.bankAccount}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>bank Branch</label>
                        <input
                          className="form-control"
                          onChange={handleChange('bankBranch')}
                          defaultValue={values.bankBranch}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Bank Account CreationDate</label>
                        <input
                          className="form-control"
                          onChange={handleChange('bankAccountCreationDate')}
                          defaultValue={values.bankAccountCreationDate}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Products</label>
                        <input
                          className="form-control"
                          onChange={handleChange('products')}
                          defaultValue={values.products}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Total</label>
                        <input
                          className="form-control"
                          onChange={handleChange('total')}
                          defaultValue={values.total}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Prepayment</label>
                        <input
                          className="form-control"
                          onChange={handleChange('prepayment')}
                          defaultValue={values.prepayment}
                          />
                      </div>
                      <div className="form-group form-check">
                        <label>Checks Number</label>
                        <input
                          className="form-control"
                          onChange={handleChange('checksNumber')}
                          defaultValue={values.checksNumber}
                          />
                      </div>
                      <button onClick={this.continue} className="btn btn-primary">Submit</button>
                      <button onClick={this.back} className="btn btn-secondary ">Bck</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default Step2
