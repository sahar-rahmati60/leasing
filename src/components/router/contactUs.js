import React, { Component } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import styles from "../../styles/contactUs.scss";

const CONTACT_MUTATION = gql`
  mutation ContactMutation(
    $name: String!
    $email: String!
    $phone: String!
    $messageBody: String!
  ) {
    contactMutation(
      name: $name
      email: $email
      phone: $phone
      messageBody: $messageBody
    ) {
      name
      email
      phone
      messageBody
    }
  }
`;
class ContactUS extends Component {
  state = {
    name: "",
    email: "",
    phone: "",
    messageBody: ""
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { name, email, phone, messageBody } = this.state;
    return (
      <section>
        <div className={`${styles.Container} ${styles.lighBox}`}>
          <div className={styles.Row}>
            <div className={styles.fullRow}>
              <h2 className={styles.contactTitle}>
                نیاز به مشاوره بیشتری دارید؟‌ سوالات خود را مطرح کنید تا در اسرع
                وقت با شما تماس بگیریم
              </h2>
              <form className={styles.contactForm}>
                <h3>فرم تماس با ما</h3>
                <div className={`${styles.Row} ${styles.mainForm}`}>
                  <div className={styles.contactTextInputsColumn}>
                    <div>
                      <input
                        defaultValue={name}
                        placeholder="نام و نام خانوادگی"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div>
                      <input
                        defaultValue={email}
                        placeholder="ایمیل"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div>
                      <input
                        defaultValue={phone}
                        placeholder="شماره تلفن همراه"
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                  <div className={styles.contactTextAreaColumn}>
                    <textarea
                      onChange={this.handleChange}
                      defaultValue={messageBody}
                      placeholder="سوالات عمومی انتقادات و پیشنهادات خود را از طریق این فرم با ما در میان بگذارید"
                      rows="3"
                    />
                  </div>
                </div>
                <div className={styles.leftAlign}>
                  <Mutation
                    mutation={CONTACT_MUTATION}
                    variables={{ name, email, phone, messageBody }}
                  >
                    {contactMutation => (
                      <button onClick={contactMutation}>ثبت و ارسال</button>
                    )}
                  </Mutation>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default ContactUS;
