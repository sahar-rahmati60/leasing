import React, { Component } from "react";
import styles from "../../../styles/calculator.scss"

class Calculator extends Component {
  state={
    showResult: false,
    showPayment:false,
    price:"",
    payment:"",
    installmentNumber:"",
    optionalPayment:"",
    percentInstallments:"",
    interestInstallments:"",
    remaining: "",
    total:" ",
    installmentAmount: "",
    totalAmount:"",
    errors:{}
  }

  handleChange = input => event => {
   this.setState({ [input] : event.target.value })
 }
 restartForm =()=>{
   this.setState({total:''})
   this.setState({installmentAmount:''})
   this.setState({totalAmount:''})
   this.setState({payment:''})
 }
 validateForm = () => {
   let errors = {};
   let formIsValid = true;
   if (!this.state.price) {
     formIsValid = false;
     errors["price"] = "مبلغ فاکتور نمی تواند خالی باشد";
   }
   if (this.state.price < 1000000) {
     formIsValid = false;
     errors["price2"] = "مبلغ فاکتور نمی تواند کمتر از یک میلیون باشد";
   }
   if (!this.state.installmentNumber) {
     formIsValid = false;
     errors["installmentNumber"] = "تعداد اقساط نمی تواند خالی باشد";
   }
   if (!this.state.optionalPayment) {
     formIsValid = false;
     errors["optionalPayment"] = "نوع پیش پرداخت نمی تواند خالی باشد";
   }
   if((this.state.optionalPayment==="1") && (!this.state.payment)){
     formIsValid = false;
     errors["payment"] = "مبلغ پیش فاکتور نمی تواند خالی باشد";
   }
   if((this.state.payment < this.state.price/4) && (this.state.optionalPayment==="1")){
     formIsValid = false;
     errors["payment2"] = "مبلغ پیش پرداخت حتما باید یک چهارم مبلغ فاکتور باشد";
   }
   if((this.state.payment > this.state.price) && (this.state.optionalPayment==="1")){
     formIsValid = false;
     errors["payment3"] = "مبلغ پیش پرداخت نمی تواند بیشتر از مبلغ فاکتور باشد";
   }
   this.setState({
    errors
  });
  return formIsValid;
 }
 //Show calculator result
  showResulthandler = () => {
    if(this.validateForm()){
      this.setState({
        showResult: true
      })
      let payment = 0
      if(this.state.optionalPayment==="0"){
          payment= parseInt(this.state.price) /4
          this.setState({
            payment
          })
      } else if (this.state.optionalPayment==="1"){
          payment= parseInt(this.state.payment)
      }
      let installmentNumber = parseInt(this.state.installmentNumber)
      let price = parseInt(this.state.price)
      let remaining = price - payment
      let percentInstallments = ( installmentNumber +1)*1.55
      let interestInstallments = ((remaining + price*0.05 + 40000) * percentInstallments )/100
      let total = Math.floor(remaining + interestInstallments + price*0.05 + 40000)
      this.setState({
        total
      })
      let installmentAmount = Math.floor(total/installmentNumber)
      this.setState({
        installmentAmount
      })
      let totalAmount = Math.floor(payment + total)
      this.setState({
        totalAmount
      })
    }
  }
  //ENDs

  // show optional payment
  showPaymenthandler = () => {
    this.setState({
      showPayment: true
    })
  }
  hiddenPaymenthandler = () => {
    this.setState({
      showPayment: false
    })
  }
  //END
  render() {
    const{closePopup}= this.props;
    const{errors,showResult,showPayment,total,totalAmount,installmentAmount,price,payment,installmentNumber,optionalPayment}= this.state;
    return (
      <div className={styles.popup}>
        <button onClick={closePopup}>
          <svg className={styles.closeIcon} xmlns="http://www.w3.org/2000/svg" id="error" viewBox="0 0 52 52">
            <path id="Path_2536" d="M26 0a26 26 0 1 0 26 26A26.029 26.029 0 0 0 26 0zm0 50a24 24 0 1 1 24-24 24.028 24.028 0 0 1-24 24z"  data-name="Path 2536"/>
            <path id="Path_2537" d="M35.707 16.293a1 1 0 0 0-1.414 0L26 24.586l-8.293-8.293a1 1 0 0 0-1.414 1.414L24.586 26l-8.293 8.293a1 1 0 1 0 1.414 1.414L26 27.414l8.293 8.293a1 1 0 0 0 1.414-1.414L27.414 26l8.293-8.293a1 1 0 0 0 0-1.414z"  data-name="Path 2537"/>
          </svg>
        </button>
        <div className={styles.CalculatorPopup}>
          <h1 className={styles.title}>فرم محاسبه اقساط</h1>
          <div className={styles.mainBox}>
            <p>توجه: این محاسبه گر صرفا جهت محاسبه  اقساط توسط شما تهیه شده</p>
            <div>
              <label>مبلغ فاکتور به تومان</label>
              <input
                type="text"
                defaultValue={price}
                onChange={this.handleChange('price')}
              />
              <div className={styles.errors}>{errors.price}</div>
              <div className={styles.errors}>{errors.price2}</div>
            </div>
            <div className={styles.financeNo}>
              <div className={styles.containerNo}>
                <span>تعداد اقساط</span>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="1"
                    checked={installmentNumber==="1"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="2"
                    checked={installmentNumber==="2"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="3"
                    checked={installmentNumber==="3"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۳</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="4"
                    checked={installmentNumber==="4"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۴</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="5"
                    checked={installmentNumber==="5"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۵</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="6"
                    checked={installmentNumber==="6"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۶</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="7"
                    checked={installmentNumber==="7"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۷</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="8"
                    checked={installmentNumber==="8"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۸</span>
                </label>
              </div>
              <div className={styles.containerNo2}>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="9"
                    checked={installmentNumber==="9"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۹</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="10"
                    checked={installmentNumber==="10"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۰</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="11"
                    checked={installmentNumber==="11"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۱</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="12"
                    checked={installmentNumber==="12"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۲</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="13"
                    checked={installmentNumber==="13"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۳</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="14"
                    checked={installmentNumber==="14"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۴</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="15"
                    checked={installmentNumber==="15"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۵</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="16"
                    checked={installmentNumber==="16"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۶</span>
                </label>
              </div>
              <div className={styles.containerNo2}>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="17"
                    checked={installmentNumber==="17"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۷</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="18"
                    checked={installmentNumber==="18"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۸</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="19"
                    checked={installmentNumber==="19"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۱۹</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="20"
                    checked={installmentNumber==="20"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲۰</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="21"
                    checked={installmentNumber==="21"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲۱</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="22"
                    checked={installmentNumber==="22"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲۲</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="23"
                    checked={installmentNumber==="23"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲۳</span>
                </label>
                <label className={styles.NumberContainer}>
                  <input
                    type="radio"
                    name="number"
                    value="24"
                    checked={installmentNumber==="24"}
                    onChange={this.handleChange('installmentNumber')}
                  />
                  <span className={styles.checkNo}>۲۴</span>
                </label>
              </div>
              <div className={styles.errors}>{errors.installmentNumber}</div>
            </div>
            <div>
              <span>پیش پرداخت</span>
              <label className={styles.radioContainer}>
                <input
                  type="radio"
                  checked={optionalPayment==="0"}
                  value="0"
                  name="suggested"
                  onClick={this.hiddenPaymenthandler}
                  onChange={this.handleChange('optionalPayment')}/>
                  پیشنهادی | پیشنهاد بر مبنای کمترین پیش پرداخت
                  <span className={styles.checkRadio} />
              </label>
              <div className={styles.option}>
                <label className={styles.radioContainer}>
                  <input
                    type="radio"
                    checked={optionalPayment==="1"}
                    value="1"
                    name="suggested"
                    onClick={this.showPaymenthandler}
                    onChange={this.handleChange('optionalPayment')}/>
                    انتخابی | برای انتخاب پیش پرداخت بالاتر
                    <span className={styles.checkRadio} />
                </label>
              </div>
              <div className={styles.errors}>{errors.optionalPayment}</div>
            </div>
            {showPayment ?
              <div className={styles.paymeny}>
                <label>مبلغ پیش پرداخت</label>
                <input
                  type="text"
                  defaultValue={payment}
                  onChange={this.handleChange('payment')}
                />
                <div className={styles.errors}>{errors.payment}</div>
                <div className={styles.errors}>{errors.payment2}</div>
                <div className={styles.errors}>{errors.payment3}</div>
              </div>
              :
              null
            }

            <div className={styles.calculateButton}>
              <button onClick={this.showResulthandler} type="button">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.448 30.227">
                    <path id="calculator" d="M23.615 0H2.834A2.905 2.905 0 0 0 0 2.834v24.559a2.905 2.905 0 0 0 2.834 2.834h20.781a2.905 2.905 0 0 0 2.834-2.834V2.834A2.905 2.905 0 0 0 23.615 0zM7.557 25.693a.813.813 0 0 1-.756.756H4.534a.813.813 0 0 1-.756-.756v-2.267a.813.813 0 0 1 .756-.756H6.8a.813.813 0 0 1 .756.756zm0-7.557a.813.813 0 0 1-.756.756H4.534a.813.813 0 0 1-.756-.756v-2.267a.813.813 0 0 1 .756-.756H6.8a.813.813 0 0 1 .756.756zm7.557 7.557a.813.813 0 0 1-.756.756h-2.267a.813.813 0 0 1-.756-.756v-2.267a.813.813 0 0 1 .756-.756h2.267a.813.813 0 0 1 .756.756zm0-7.557a.813.813 0 0 1-.756.756h-2.267a.813.813 0 0 1-.756-.756v-2.267a.813.813 0 0 1 .756-.756h2.267a.813.813 0 0 1 .756.756zm7.557 7.557a.813.813 0 0 1-.756.756h-2.268a.813.813 0 0 1-.756-.756v-9.824a.813.813 0 0 1 .756-.756h2.267a.813.813 0 0 1 .756.756v9.824zm0-15.113a.813.813 0 0 1-.756.756H4.534a.813.813 0 0 1-.756-.756V4.534a.813.813 0 0 1 .756-.756h17.38a.813.813 0 0 1 .756.756z" />
                </svg>
                <span>محاسبه کن</span>
              </button>
            </div>
            {showResult ?
              <div className={styles.resultBox}>
                <table>
                  <tbody>
                    <tr>
                      <td>پیش پرداخت</td>
                      <td>{payment}</td>
                    </tr>
                    <tr>
                      <td>مبلغ قسط</td>
                      <td>{installmentAmount}</td>
                    </tr>
                    <tr>
                      <td>جمع کل اقساط</td>
                      <td>{total}</td>
                    </tr>
                    <tr>
                      <td>قیمت تمام شده کالا</td>
                      <td>{totalAmount}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            :null
            }
            <div className={styles.refreshForm}>
              <button onClick={this.restartForm}>محاسبه مجدد</button> |
              <button onClick={closePopup}>بستن فرم محاسبه</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
