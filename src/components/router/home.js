import React, { Component } from "react";
import { Link } from "react-router-dom";

import ContactUS from "./contactUs";
import Calculator from "./calculator/calculator"
import styles from "../../styles/home.scss";

class Home extends Component {
  state={
    showPopup: false
  }
  showPopupBox=()=>{
    const{showPopup} = this.state;
    this.setState({
      showPopup: !showPopup
    })
  }
  render() {
    return (
      <React.Fragment>
        <section>
          <div className={`${styles.Container} ${styles.financeDec}`}>
            <div className={styles.Row}>
              <div className={styles.halfRaow}>
                <h2>فروش اقساطی محصولات تهران ملودی برای تمام ایران</h2>
                <p>
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                  استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                  مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                  تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                  کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                  آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم
                  افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                  طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این
                  صورت می توان امید داشت که تمام و دشواری موجود در ارائه
                  راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل
                  حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای
                  موجود طراحی اساسا مورد استفاده قرار گیرد.
                </p>
                <Link className={styles.request} to={`/status`}>
                  پیگیری درخواست های قبلی با کد رهگیری
                </Link>
              </div>
              <div className={`${styles.halfRaow} ${styles.leftAlign}`}>
                <img alt="pic" src={require("../../Images/pic1.png")} />
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className={`${styles.Container} ${styles.lighBox}`}>
            <div className={styles.Row}>
              <div className={`${styles.halfRaow} ${styles.middleAlign}`}>
                <img
                  alt="Iran Renter"
                  className={styles.otherLogo}
                  src={require("../../Images/LogoFinance.png")}
                />
                <span className={styles.financeTitle}>
                  خرید اقساط از تهران ملودی
                </span>
              </div>
              <div className={`${styles.halfRaow} ${styles.justifyEndMiddle}`}>
                <Link to="/request" className={styles.financeLink}>
                  خرید اقساطی از تهران ملودی
                </Link>
              </div>
            </div>
            <div className={styles.Row}>
              <div className={styles.fullRow}>
                <h4>مراحل انجام خرید</h4>
                <ol>
                  <li>
                    فرم اقساط را داخل سایت تکمیل نمایید. « در صورت دریافت کد
                    رهگیری، ثبت شما با موفقیت انجام شده است»
                  </li>
                  <li>
                    با توجه به اطلاعات ثبت شده در فرم، حساب جاری معرفی شده توسط
                    شما، استعلام می گردد
                  </li>
                  <li>
                    نتیجه استعلام پس از سه روز کاری داخل سایت قرار داده می شود.
                  </li>
                  <li>
                    با کد رهگیری، که هنگام ثبت فرم اقساط دریافت نموده اید به
                    لینک «برای پیگیری درخواست خود کلیک کنید» مراجعه نموده و
                    نتیجه{" "}
                  </li>
                  <li>
                    در صورت تایید با تلفن فروشگاه «66171517-021» تماس گرفته و با
                    کارشناس اقساط، جهت خرید (حضوری-غیرحضوری) و آگاهی از مدارک
                    مورد نیاز، هماهنگ نمایید.
                  </li>
                </ol>
              </div>
            </div>
            <div className={styles.Row}>
              <div className={styles.halfRaow}>
                <button
                  className={styles.calculator}
                  onClick={this.showPopupBox}>
                  فرم محاسبه اقساط
                </button>
                {this.state.showPopup ?
                  <Calculator
                    text='Close Me'
                    closePopup={this.showPopupBox}
                  />
                  : null
                }
              </div>
              <div className={`${styles.halfRaow} ${styles.justifyEndMiddle}`}>
                <Link to="/request">خرید اقساطی از تهران ملودی</Link>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className={`${styles.Container} ${styles.lighBox}`}>
            <div className={styles.Row}>
              <div className={`${styles.halfRaow} ${styles.middleAlign}`}>
                <img
                  alt="Iran Renter"
                  className={styles.otherLogo}
                  src={require("../../Images/irnrenter.png")}
                />
                <span className={styles.financeTitle}>
                  خرید اقساط از ایران رنتر
                </span>
              </div>
              <div className={`${styles.halfRaow} ${styles.justifyEndMiddle}`}>
                <a
                  className={styles.financeLink}
                  href="https://iranrenter.com/search/category-credit-purchase-card/tehranmelody?brands%5B%5D=178"
                >
                  خرید اقساطی از ایران رنتر
                </a>
              </div>
            </div>
            <div className={styles.Row}>
              <div className={styles.fullRow}>
                <p>
                  از آنجایی که خرید اقساطی یک راه‌حل همیشگی برای خرید وسایل مورد
                  نیاز افراد بوده است و با توجه به پیشرفت تکنولوژی و متعاقب آن،
                  اهمیت زمان در دنیای مدرن امروزی، توانستیم ایران‌رنتر که متشکل
                  از تیمی 80 نفره از متخصصین است را به فروشگاهی آنلاین برای فروش
                  اقساطی تبدیل کنیم که طیف وسیعی از وسایل را پوشش داده و همچنان
                  در حال گسترش این وسایل است. سهولت و سرعت بیشتر در ارائه خدمات
                  و اعتبارسنجی افراد که همه در فرآیندی کاملاً آنلاین و بدون نیاز
                  به مراجعه حضوری صورت می‌گیرد یکی از اهداف مهم ما در راستای
                  رضایت بیشتر مشتریان است و برای آن به امتحان روش‌های جدید و به
                  روز می‌پردازیم. ما به نیاز مشتریان می‌اندیشیم و به دنبال
                  راه‌هایی نوآورانه و راحت برای خرید اقساطی و ارائه خدمت هر چه
                  بهتر به آن‌ها هستیم.
                </p>
                <h4>
                  مراحل خرید کارت‌های اعتباری خرید تهران ملودی به صورت زیر است
                </h4>
                <ol>
                  <li>تهیه کارت اعتباری خرید تهران ملودی از ایران‌رنتر</li>
                  <li>تکمیل فرآیند اعتبارسنجی و دریافت کارت</li>
                  <li>خرید از تهران ملودی</li>
                  <li>
                    پرداخت اقساط در ایران‌رنتر برای تهیه کارت‌های اعتباری خرید
                    تهران ملودی، مانند دیگر کالاهای موجود در سایت باید ابتدا
                    فرآیند ثبت و اعتبارسنجی را طی کنید. با تهیه کارت اعتباری
                    خرید تهران ملودی می‌توانید به میزان اعتبارِ کارت خریداری‌شده
                    و با استفاده از کد ارائه‌شده همراه کارت از سایت تهران ملودی
                    هر آنچه نیاز دارید را به صورت نقدی خریداری کرده و هزینه را
                    به صورت اقساطی در سایت ایران‌رنتر پرداخت کنید. فروش اقساطی
                    کارت اعتباری خرید تهران ملودی 4 میلیون تومانی می‌تواند یک
                    هدیه عالی برای کسانی باشد که از علاقه‌مندان و عاشقان دنیای
                    موسیقی هستند. فروش اقساطی کارت اعتباری خرید تهران ملودی 4
                    میلیون تومانی، خدمت ویژه ایران‌رنتر در جهت افزایش قدرت خرید
                    همه افراد جامعه است. شما با استفاده از کارت اعتباری خرید
                    تهران ملودی می‌توانید هر مدل ساز و ادوات موسیقی، لوازم
                    استودیو و تجهیزات صدابرداری را با بهترین شرایط تهیه کنید. پس
                    اگر برای خرید کارت‌های اعتباری خرید تهران ملودی مشتاق هستید،
                    این فرصت را از دست ندهید و با خرید اقساطی این کارت های
                    اعتباری از دنیای موسیقی با خیال راحت لذت ببرید
                  </li>
                </ol>
                <div className={styles.leftAlign}>
                  <a href="https://iranrenter.com/search/category-credit-purchase-card/tehranmelody?brands%5B%5D=178">
                    خرید اقساط از ایران رنتر
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <ContactUS />
        <div className={styles.phoneContainer}>
          <p className={styles.title}>با ما تماس بگیرید</p>
          <p className={styles.phone}>۰۲۱-۶۶۱۷۱۵۱۷</p>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;
