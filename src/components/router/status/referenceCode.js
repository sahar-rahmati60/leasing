import React,{Component} from 'react';

import styles from '../../../styles/status.scss';

class ReferenceCode extends Component {

    render() {
      const {handleChange,referenceCode,goStatusPage}= this.props;
        return (
            <div className={styles.status}>
              <h1>پیگیری درخواست های خرید اقساط</h1>
              <p>
                برای پیگیری درخواست خرید اقساط، کد رهگیری که پس از تکمیل فرم اقساط
                دریافت کرده‌اید و به آدرس ایمیل شما نیز ارسال شده را در کادر زیر وارد کنید
              </p>
              <input
                onChange={handleChange('referenceCode')}
                placeholder="کد رهگیری"
                defaultValue={referenceCode}/>
              <button className={styles.statusButton} onClick={goStatusPage}>
                  بررسی وضعیت درخواست
                </button>
            </div>
        );
    }
}

export default ReferenceCode
