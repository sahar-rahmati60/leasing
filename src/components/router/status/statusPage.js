import React,{Component} from 'react'
import { Link } from "react-router-dom";

import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import styles from '../../../styles/status.scss';

const FEED_QUERY = gql`
 query financeRequestStatus($referenceCode: String!) {
   financeRequestStatus(referenceCode: $referenceCode){
     firstName
   }
 }
`
class StatusPage extends Component {
    render() {
      const referenceCode = this.props.referenceCode;
      return (
        <div className={styles.container}>
          <div className={styles.row}>
            <div className={styles.columnOne}>
              <div className={styles.statusPage}>
                <h1>پیگیری درخواست های خرید اقساط</h1>
                <p>برای پیگیری درخواست خرید اقساط، کد رهگیری که پس از تکمیل فرم اقساط
                  دریافت کرده‌اید و به آدرس ایمیل شما نیز ارسال شده را در کادر زیر وارد کنید</p>
                <div className={styles.refreshCode}>
                  <Query query={FEED_QUERY} variables={{ referenceCode }}>
                    {({ loading, error ,data}) => {
                      if (loading) return (
                        <div className={styles.showFetching} >
                          <div className={styles.showBgAnimate}></div>
                        </div>
                      )
                      if (error) return <div className={styles.statusText}>Error</div>
                      if(!data.financeRequestStatus)
                      return (
                        <div className={styles.showStatus}>کد رهگیری را اشتباه وارد کرده اید</div>
                      )
                      return (
                        <div className={styles.showStatus}>{data.financeRequestStatus.firstName}</div>
                      )
                    }}
                  </Query>
                  <button onClick={this.props.goBack}>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.666 19.654">
                      <path id="redo"
                        d="M27.2 0h-1.85a.468.468 0 0 0-.467.49l.155 3.227a9.671 9.671 0 1 0-.886
                        13.444.469.469 0 0 0 .019-.68l-1.325-1.325a.467.467 0 0 0-.639-.021 6.864
                        6.864 0 1 1 1.162-8.983l-3.959-.19a.468.468 0 0 0-.49.467V8.28a.468.468 0 0 0
                        .468.468H27.2a.468.468 0 0 0 .468-.468V.468A.468.468 0 0 0 27.2 0z"
                        transform="translate(-8)"/>
                    </svg>
                    <span>پیگیری کد جدید</span>
                  </button>
                  <Link className={styles.request} to={`/request`}>
                    بازگشت به صفحه اقساط
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
}

export default StatusPage
