import React, { Component } from "react";

import FinanceForm from "./finance/financeform";
import Navigation from "./finance/navigation";
import styles from "../../styles/finance.scss";

class Finance extends Component {
  state={
    step: 1,
    active: false
  }

  // go to next Level
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });

  };

  stepTwo = () => {
    this.setState({
      step: 1 + 1,
    });
  }
  stepThree = () => {
    this.setState({
      step: 2 + 1,
    });
  }
  stepFour = () => {
    this.setState({
      step: 3 + 1,
    });
  }
  // go back previus level
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };
  render() {
    const {step} = this.state;
    return (
      <div className={styles.pageContainer}>
        <div className={styles.formRow}>
          <div className={styles.formColumnOne}>
            <h2 className={styles.pageTitle}>ثبت نام خرید اقساطی از تهران ملودی</h2>
            <Navigation
              step={step}
            />
            <FinanceForm
              step={step}
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              stepTwo={this.stepTwo}
              stepThree = {this.stepThree}
              stepFour = {this.stepFour}
              />
          </div>
        </div>
      </div>
    );
  }
}

export default Finance;
