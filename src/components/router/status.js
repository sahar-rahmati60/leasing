import React, { Component } from "react";

import ReferenceCode from './status/referenceCode'
import StatusPage from './status/statusPage'

class Status extends Component {
  state = {
    referenceCode:'',
    isShow: true,
  };
  handleChange = input => event =>{
    this.setState({referenceCode : event.target.value})
  }
  goStatusPage=()=>{
    this.setState({isShow:false})
  }
  goBack=()=>{
    this.setState({isShow:true})
    this.setState({referenceCode:''})
  }
  showForm=()=>{
    const{isShow,referenceCode}= this.state;
    if (isShow) {
        return (
          <ReferenceCode
            handleChange={this.handleChange}
            goStatusPage={this.goStatusPage}
            referenceCode={referenceCode}
            />
        )
    } else {
        return (
          <StatusPage
            referenceCode = {referenceCode}
            goBack = {this.goBack}

          />
        )
    }
  };
  render() {
      return (
          <div>
            {this.showForm()}
          </div>
      )
  }
}
export default Status;
