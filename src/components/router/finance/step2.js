import React, { Component } from "react";
import styles from "../../../styles/finance.scss";

import Select from 'react-select';

const city = [
  { value: 'تهران', label: 'تهران' },
  { value: 'تبریز', label: 'تبریز' },
  { value: 'مشهد', label: 'مشهد' }
];
const province = [
  { value: 'تهران', label: 'تهران' },
  { value: 'آذربایجان شرقی', label: 'آذربایجان شرقی' },
  { value: 'خراسان رضوی', label: 'خراسان رضوی' }
];
const year = [
  { value: '1362', label: '۱۳۶۲' },
  { value: '1362', label: '۱۳۶۲' },
  { value: '1362', label: '۱۳۶۲' }
];
const month = [
  { value: '11', label: '11' },
  { value: '10', label: '12' },
  { value: '12', label: '10' }
];
const day = [
  { value: '11', label: '۱۱' },
  { value: '12', label: '۱۲' },
  { value: '13', label: '۱۳' }
];
const customStyles = {
  option: (base, state) => ({
    ...base,
    backgroundColor:'#f7f7f7',
    padding : 10,
    color: '#d2d2d2',
    fontSize:16,
    '&:hover': {
      backgroundColor: '#f1f1f1',
    }
  }),
  indicatorsContainer:()=> ({
    borderRight: 0,
  }),
  control: (base, state) => ({
     ...base,
      borderColor: '#f1f1f1',
      height:50,
      '&:hover': { borderColor: '#f1f1f1' }, // border style on hover
      boxShadow: 'none',
      marginBottom: 35,
   }),
   singleValue:()=>({
     color: '#9e9e9e',
   })
}

class Step2 extends Component {
  state={
    errors:{}
  }
  validateForm = () => {
    let {values} = this.props;
    let errors = {};
    let formIsValid = true;
    if (!values.firstName) {
      formIsValid = false;
      errors["firstName"] = "نام نمی‌تواند خالی باشد.";
    }
    if (!values.lastName) {
      formIsValid = false;
      errors["lastName"] = "نام خانوادگی نمی‌تواند خالی باشد.";
    }
    if (!values.fatherName) {
      formIsValid = false;
      errors["fatherName"] = "نام پدر نمی تواند خالی باشد";
    }

    if(!values.nationalCode.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["nationalCode2"] = "کدملی باید اعداد انگلیسی باشد و پرکردن این فیلد ضروری است";
    }
    if(!values.idNumber.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["idNumber2"] = "شماره شناسنامه باید اعداد انگلیسی باشد و پر کردن این فیلد ضروری است";
    }

    if(typeof values.email !== "undefined"){
       let lastAtPos = values.email.lastIndexOf('@');
       let lastDotPos = values.email.lastIndexOf('.');

       if (!(lastAtPos < lastDotPos && lastAtPos > 0 && values.email.indexOf('@@') === -1 && lastDotPos > 2 && (values.email.length - lastDotPos) > 2)) {
          formIsValid = false;
          errors["email2"] = "آدرس ایمیل نامعتبر است و پرکردن این فیلد ضروری است";
      }
    }
    if (!values.marriageStatus) {
      formIsValid = false;
      errors["marriageStatus"] = "لطفا وضعیت تاهل را انتخاب کنید";
    }
    if (!values.selectBirthYear) {
      formIsValid = false;
      errors["selectBirthYear"] = "*Please enter your selectBirthYear.";
    }
    if (!values.selectBirthMonth) {
      formIsValid = false;
      errors["selectBirthMonth"] = "*Please enter your selectBirthMonth.";
    }

    if(!values.mobile.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["mobile2"] = "شماره موبایل باید اعداد انگلیسی باشد و نمی تواند خالی باشد ";
    }

    if(!values.phone.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["phone2"] = "شماره تلفن باید اعداد انگلیسی باشد و پرکردن این فیلد ضروری است";
    }
    if (!values.company) {
      formIsValid = false;
      errors["company"] = "نام شرکت نمی تواند خالی باشد ";
    }
    if (!values.jobTitle) {
      formIsValid = false;
      errors["jobTitle"] = "عنوان شغلی نمی تواند خالی باشد";
    }

    if(!values.jobPhone.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["jobPhone2"] = "شماره تلفن باید اعداد انگلیسی باشد و پرکردن این فیلد ضروری است";
    }

    if (!values.addressWork) {
      formIsValid = false;
      errors["addressWork"] = "آدرس نمی تواند خالی باشد لطفا شهر و استان را انتخاب کنید";
    }

    if (!values.addressHome) {
      formIsValid = false;
      errors["addressHome"] = "آدرس نمی تواند خالی باشد لطفا شهر و استان را انتخاب کنید ";
    }
    this.setState({
     errors
   });
   return formIsValid;
  }
  continue2 = (e) => {
    e.preventDefault();
    if(this.validateForm()){
      this.props.continue();
    }
  };
  render() {
    const {values,handleCity,handleProvince,handleCityWork,handleProvinceWork,handleChange,handleRadioButton,
            handleDay,handleMonth,handleYear} = this.props;
    const {errors} = this.state;
    return (
      <div className={styles.pageContainer}>
        <div className={styles.formRow}>
          <div className={styles.contentContainer}>
            <div className={styles.body}>
              <h2>
                اطلاعات فردی
                <span className={styles.subtitle}>
                  همانطور که در شرایط و قوانین مطالعه کردید در این بخش باید
                  اطلاعات <span>صاحب چک</span> را به صورت دقیق وارد کنید.
                </span>
              </h2>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneThird}>
                  <label>نام</label>
                  <input
                    onChange={handleChange('firstName')}
                    defaultValue={values.firstName}
                    type="text"
                    name="firstName"
                  />
                <div className={styles.errors}>{errors.firstName}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <label>نام خانوادگی</label>
                  <input
                    onChange={handleChange('lastName')}
                    defaultValue={values.lastName}
                    type="text"
                    name="lastName"
                  />
                <div className={styles.errors}>{errors.lastName}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <label>نام پدر</label>
                  <input
                    type="text"
                    onChange={handleChange('fatherName')}
                    defaultValue={values.fatherName}
                  />
                <div className={styles.errors}>{errors.fatherName}</div>
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneThird}>
                  <label>کد ملی</label>
                  <input
                    type="text"
                    onChange={handleChange('nationalCode')}
                    defaultValue={values.nationalCode}
                  />
                <div className={styles.errors}>{errors.nationalCode}</div>
                <div className={styles.errors}>{errors.nationalCode2}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <label>شماره شناسنامه</label>
                  <input
                    type="text"
                    onChange={handleChange('idNumber')}
                    defaultValue={values.idNumber}
                  />
                <div className={styles.errors}>{errors.idNumber}</div>
                <div className={styles.errors}>{errors.idNumber2}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <label>آدرس ایمیل</label>
                  <input
                    type="text"
                    onChange={handleChange('email')}
                    defaultValue={values.email}
                  />
                <div className={styles.errors}>{errors.email2}</div>
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={`${styles.formColumnFive} ${styles.marriageSelect}`}>
                  <span className={styles.marriageTitle}>وضعیت تاهل</span>
                  <label className={styles.radioContainer}>
                    متاهل
                    <input
                      type="radio"
                      checked={values.marriageStatus === "0"}
                      name="radio"
                      onChange={handleRadioButton}
                      value="0"
                    />
                    <span className={styles.checkRadio} />
                  </label>
                  <label className={styles.radioContainer}>
                    مجرد
                    <input
                      type="radio"
                      checked={values.marriageStatus === "1"}
                      name="radio"
                      onChange={handleRadioButton}
                      value="1"
                    />
                    <span className={styles.checkRadio} />
                  </label>
                  <div className={styles.errorsMarriage}>{errors.marriageStatus}</div>
                </div>
                <div className={styles.formColumnSeven}>
                  <div className={styles.formRow}>
                    <div className={styles.birthdayTitle}>‌
                      <span> تاریخ تولد </span>
                    </div>
                    <div className={styles.formColumnOneFour}>
                      <Select
                        value={values.selectBirthYear}
                        onChange={handleYear}
                        options={year}
                        placeholder="سال "
                        styles={customStyles}
                        />
                    </div>
                    <div className={styles.formColumnOneFour}>
                      <Select
                        value={values.selectBirthMonth}
                        onChange={handleMonth}
                        options={month}
                        placeholder="ماه"
                        styles={customStyles}
                        />
                    </div>
                    <div className={styles.formColumnOneFour}>
                      <Select
                        value={values.selectBirthDay}
                        onChange={handleDay}
                        options={day}
                        placeholder="روز"
                        styles={customStyles}
                        />
                      <div className={styles.errors}>{errors.selectBirthDay}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneTWo}>
                  <label>شماره همراه</label>
                  <input
                    type="text"
                    onChange={handleChange('mobile')}
                    defaultValue={values.mobile}
                  />
                <div className={styles.errors}>{errors.mobile}</div>
                <div className={styles.errors}>{errors.mobile2}</div>
                </div>
              </div>
              <hr />
              <h2>مشخصات محل کار</h2>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneThird}>
                  <input
                    type="text"
                    placeholder="نام شرکت"
                    className="form-control"
                    onChange={handleChange('company')}
                    defaultValue={values.company}
                  />
                <div className={styles.errors}>{errors.company}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <input
                    type="text"
                    placeholder="سمت شما"
                    className="form-control"
                    onChange={handleChange('jobTitle')}
                    defaultValue={values.jobTitle}
                  />
                <div className={styles.errors}>{errors.jobTitle}</div>
                </div>
                <div className={styles.formColumnOneThird}>
                  <input
                    type="text"
                    placeholder="شماره تلفن شرکت"
                    className="form-control"
                    onChange={handleChange('jobPhone')}
                    defaultValue={values.jobPhone}
                  />
                <div className={styles.errors}>{errors.jobPhone2}</div>
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneThird}>
                  <Select
                      value={values.selectedCityWork}
                      onChange={handleCityWork}
                      options={city}
                      placeholder="شهر"
                      styles={customStyles}
                    />
                </div>
                <div className={styles.formColumnOneThird}>
                  <Select
                      value={values.selectProvinceWork}
                      onChange={handleProvinceWork}
                      options={province}
                      placeholder="استان"
                      styles={customStyles}
                    />
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={styles.formColumnOne}>سکونت
                  <input
                    name="jobAddress"
                    placeholder="آدرس محل کار"
                    type="text"
                    onChange={handleChange('addressWork')}
                    defaultValue={values.addressWork}
                  />
                <div className={styles.errors}>{errors.addressWork}</div>
                </div>
              </div>
              <hr/>
              <h2>مشخصات منزل</h2>
              <div className={styles.formRow}>
                <div className={styles.formColumnOneThird}>
                  <Select
                      value={values.selectedCity}
                      onChange={handleCity}
                      options={city}
                      placeholder="شهر"
                      styles={customStyles}
                    />
                </div>
                <div className={styles.formColumnOneThird}>
                  <Select
                      value={values.selectProvince}
                      onChange={handleProvince}
                      options={province}
                      placeholder="استان"
                      styles={customStyles}
                    />
                </div>
                <div className={styles.formColumnOneThird}>
                  <input
                    placeholder="شماره تلفن منزل"
                    onChange={handleChange('phone')}
                    defaultValue={values.phone}
                    type="text"
                  />
                <div className={styles.errors}>{errors.phone2}</div>
                </div>
              </div>
              <div className={styles.formRow}>
                <div className={styles.formColumnOne}>
                  <input
                    name="addressHome"
                    placeholder="آٔدرس محل سکونت"
                    type="text"
                    onChange={handleChange('addressHome')}
                    defaultValue={values.addressHome}
                  />
                <div className={styles.errors}>{errors.addressHome}</div>
                </div>
              </div>
            </div>
            <div className={styles.footer}>
              <button onClick={this.continue2} className={styles.nextStepBtn}>
                مرحله بعد
              </button>
              <button onClick={this.props.back} className={styles.prevStepBtn}>
                بازگشت به شرایط و قوانین
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Step2;
