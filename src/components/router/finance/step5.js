import React, { Component } from "react";
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment-jalaali';
import styles from "../../../styles/finance.scss";

const POST_DATA = gql`
  mutation CreateRequest(
    $firstName: String!,
    $lastName : String!,
    $fatherName: String!,
    $birthday : String!,
    $marriageStatus : String!,
    $nationalCode: String!,
    $idNumber : String!,
    $mobile: String!,
    $address:String!,
    $phone:String!,
    $email: String!,
    $company:String!,
    $jobTitle:String!,
    $jobAddress: String!,
    $jobPhone:String!,
    $bankAccount:String!,
    $bank:String!,
    $bankAccountCreationDate:String!,
    $bankBranch:String!,
    $products:String!,
    $total:String!,
    $prepayment:String!,
    $checksNumber:String!) {
    createFinanceRequest(
      firstName: $firstName,
      lastName : $lastName,
      fatherName:$fatherName,
      birthday :$birthday,
      marriageStatus :$marriageStatus,
      nationalCode:$nationalCode,
      idNumber :$idNumber,
      mobile:$mobile,
      address:$address,
      phone:$phone,
      email:$email,
      company:$company,
      jobTitle:$jobTitle,
      jobAddress:$jobAddress,
      jobPhone:$jobPhone,
      bankAccount:$bankAccount,
      bank:$bank,
      bankAccountCreationDate:$bankAccountCreationDate,
      bankBranch:$bankBranch,
      products:$products,
      total:$total,
      prepayment:$prepayment,
      checksNumber:$checksNumber) {
      id
      firstName
      lastName
      fatherName
      birthday
      marriageStatus
      nationalCode
      idNumber
      mobile
      address
      phone
      email
      company
      jobTitle
      jobAddress
      jobPhone
      bankAccount
      bank
      bankAccountCreationDate
      bankBranch
      products
      total
      prepayment
      checksNumber
    }
  }
`
class Step5 extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const {values:{selectBirthDay,selectBirthMonth,selectBirthYear,firstName,lastName,fatherName,marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
      jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber}} = this.props;
      let birthdayDate = `${selectBirthYear.value} - ${selectBirthMonth.value}- ${selectBirthDay.value}`;
      let marriagFeild = marriageStatus === '0' ? 'مجرد': 'متاهل';
      //convert date to timestamp
      let m = moment(birthdayDate, 'jYYYY/jM/jD')
      let myDate = m.format('YYYY/M/D');
      console.log(myDate)
      let birthday= new Date(myDate).getTime()/1000;
      console.log(birthday)
      //end
    return (
      <div className={styles.pageContainer}>
        <div className={styles.contentContainer}>
          <div className={`${styles.body} ${styles.inputSameStyles} ${styles.finallStep}`}>
            <h2>
              بازبینی و ثبت در خواست
              <span className={styles.subtitle}>
                اطلاعات درخواست تان را بررسی کنید و سپس اقدام به ثبت سفارش کنید
              </span>
            </h2>
            <div className={styles.formRow}>
              <div className={styles.formColumnOne}>
                <div className={styles.bgGrey}>
                  <div onClick={this.props.handlestepTwo} className={styles.editBox}>
                    <h2 className={styles.titleBg}>اطلاعات فردی</h2>
                    <div className={styles.buttonEdit}>
                      <span className={styles.editIcon}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.992 19.99">
                          <path id="pen"
                            d="M11.342 3.648l5 5L5.486 19.5l-4.457.492A.937.937 0 0 1 0 18.958l.5-4.46
                            10.842-10.85zm8.09-.744L17.085.557a1.875 1.875 0 0 0-2.652 0l-2.208
                            2.208 5 5 2.208-2.208a1.875 1.875 0 0 0 0-2.652z"/>
                        </svg>
                      </span>
                      <span className={styles.editTitle}>بازبینی و ویرایش</span>
                    </div>
                  </div>
                  <div>
                    <ul>
                      <li>
                        <span>نام : {firstName} </span>
                        <span>{this.props.formData}</span>
                      </li>
                      <li>
                        <span>نام خانوادگی : {lastName}</span>
                        <span />
                      </li>
                      <li>
                        <span>نام پدر : {fatherName} </span>
                        <span />
                      </li>
                      <li>
                        <span>کدملی : {nationalCode}</span>
                        <span />
                      </li>
                      <li>
                        <span>شماره شناسنامه : {idNumber}</span>
                        <span />
                      </li>
                    </ul>
                  </div>
                  <div>
                    <ul>
                      <li>
                        <span>آدرس ایمیل: {email} </span>
                        <span />
                      </li>
                      <li>
                        <span>وضعیت تاهل : {marriagFeild}</span>
                        <span />
                      </li>
                      <li>
                        <span>شماره همراه : {mobile}</span>
                        <span />
                      </li>
                    </ul>
                  </div>
                  <div>
                    <ul>
                      <li>
                        <span>تاریخ تولد : {birthdayDate}</span>
                        <span />
                      </li>
                    </ul>
                  </div>
                  <div>
                    <ul>
                      <li>
                        <span>نام شرکت : {company}</span>
                        <span />
                      </li>
                      <li>
                        <span>سمت : {jobTitle}</span>
                        <span />
                      </li>
                      <li>
                        <span>شماره تماس شرکت : {jobPhone}</span>
                        <span />
                      </li>
                    </ul>
                  </div>
                  <div className={styles.detailsBox}>
                    <span>مشخصات محل کار: {jobAddress}</span>
                  </div>
                  <div className={styles.detailsBox}>
                    <span>مشخصات منزل: {address}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.formRow}>
              <div className={styles.formColumnOne}>
                <div className={styles.bgGrey}>
                  <div onClick={this.props.handlestepThree} className={styles.editBox}>
                    <h2 className={styles.titleBg}>اطلاعات حساب</h2>
                    <div className={styles.buttonEdit}>
                      <span className={styles.editIcon}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.992 19.99">
                          <path id="pen"
                            d="M11.342 3.648l5 5L5.486 19.5l-4.457.492A.937.937 0 0 1 0 18.958l.5-4.46
                            10.842-10.85zm8.09-.744L17.085.557a1.875 1.875 0 0 0-2.652 0l-2.208
                            2.208 5 5 2.208-2.208a1.875 1.875 0 0 0 0-2.652z"/>
                        </svg>
                      </span>
                      <span className={styles.editTitle}>بازبینی و ویرایش</span>
                    </div>
                  </div>
                  <div className={styles.detailsBox}>
                    <span>شماره حساب جاری : {bankAccount}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>نام بانک : {bank}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>سال افتتاح حساب : {bankAccountCreationDate}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>نام و کد شعبه :{bankBranch}</span>
                    <span />
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.formRow}>
              <div className={styles.formColumnOne}>
                <div className={styles.bgGrey}>
                  <div onClick={this.props.handlestepFour} className={styles.editBox}>
                    <h2 className={styles.titleBg}>اطلاعات خرید</h2>
                    <div className={styles.buttonEdit}>
                      <span className={styles.editIcon}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.992 19.99">
                          <path id="pen"
                            d="M11.342 3.648l5 5L5.486 19.5l-4.457.492A.937.937 0 0 1 0 18.958l.5-4.46
                            10.842-10.85zm8.09-.744L17.085.557a1.875 1.875 0 0 0-2.652 0l-2.208
                            2.208 5 5 2.208-2.208a1.875 1.875 0 0 0 0-2.652z"/>
                        </svg>
                      </span>
                      <span className={styles.editTitle}>بازبینی و ویرایش</span>
                    </div>
                  </div>
                  <div className={styles.detailsBox}>
                    <span>نام محصولات: {products}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>جمع فاکتور به تومان : {total}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>مبلغ پیش پرداخت : {prepayment}</span>
                    <span />
                  </div>
                  <div className={styles.detailsBox}>
                    <span>تعداد اقساط : {checksNumber}</span>
                    <span />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.footer}>
            <Mutation  mutation={POST_DATA}  variables={{firstName,lastName,fatherName,birthday,
              marriageStatus,nationalCode,idNumber,mobile,address,phone,email,company,jobTitle,
              jobAddress,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,
              total,prepayment,checksNumber}}>
              {createFinanceRequest => <button className={styles.nextStepBtn} onClick={createFinanceRequest}>ثبت درخواست خرید اقساط</button>}
            </Mutation>
          </div>
        </div>
      </div>
    );
  }
}

export default Step5;
