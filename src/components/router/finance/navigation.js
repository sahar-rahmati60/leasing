import React, { Component } from "react";
import styles from '../../../styles/navigation.scss';


class Navigation extends Component {

  render() {
    const {step}= this.props;
    return (
      <ul className={styles.navigation}>
        <li className= {`${step === 1 ? `${styles.activeStep}` : `${styles.activeNo}`}
                        ${step < 2 ? `${styles.activeNo}` : `${styles.activeGreen}`}`}>
          <span className={styles.navIcons}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.452 31.27">
              <path id="file-alt" d="M13.68,8.306V0H1.466A1.462,1.462,0,0,0,0,1.466V29.8A1.462,1.462,0,0,0,1.466,31.27H21.986A1.462,1.462,0,0,0,23.452,29.8V9.772H15.146A1.47,1.47,0,0,1,13.68,8.306Zm3.909,14.413a.735.735,0,0,1-.733.733H6.6a.735.735,0,0,1-.733-.733v-.489A.735.735,0,0,1,6.6,21.5h10.26a.735.735,0,0,1,.733.733Zm0-3.909a.735.735,0,0,1-.733.733H6.6a.735.735,0,0,1-.733-.733v-.489a.735.735,0,0,1,.733-.733h10.26a.735.735,0,0,1,.733.733Zm0-4.4V14.9a.735.735,0,0,1-.733.733H6.6a.735.735,0,0,1-.733-.733v-.489A.735.735,0,0,1,6.6,13.68h10.26A.735.735,0,0,1,17.589,14.413Zm5.863-6.968v.373H15.635V0h.373a1.465,1.465,0,0,1,1.038.428l5.979,5.985A1.461,1.461,0,0,1,23.452,7.445Z"/>
            </svg>
          </span>
          <span className={styles.navigationTitle}>شرایط و قوانین</span>
        </li>
        <li className= {`${step === 2 ? `${styles.activeStep}` : `${styles.activeNo}`}
                        ${step <3 ? `${styles.activeNo}` : `${styles.activeGreen}`}`}>
          <span className={styles.navIcons}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.809 31.809">
              <path id="user-alt" d="M15.9 17.892a8.946 8.946 0 1 0-8.942-8.946
                8.948 8.948 0 0 0 8.942 8.946zm7.952 1.988h-3.419a10.815 10.815 0 0 1-9.058 0H7.952A7.952
                7.952 0 0 0 0 27.833v.994a2.983 2.983 0 0 0 2.982 2.982h25.845a2.983 2.983 0 0 0
                 2.982-2.982v-.994a7.952 7.952 0 0 0-7.953-7.953z"/>
            </svg>
          </span>
          <span className={styles.navigationTitle}> اطلاعات فردی</span>
        </li>
        <li className= {`${step === 3 ? `${styles.activeStep}` : `${styles.activeNo}`}
                        ${step < 4 ? `${styles.activeNo}` : `${styles.activeGreen}`}`}>
          <span className={styles.navIcons}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37.84 22.703">
              <path id="money-bill" d="M35.948 64H1.892A1.892 1.892 0 0 0 0 65.892v18.92A1.892 1.892 0 0 0 1.892 86.7h34.056a1.892 1.892 0 0 0 1.892-1.892V65.892A1.892 1.892 0 0 0 35.948 64zM2.838 83.866v-3.784a3.784 3.784 0 0 1 3.784 3.784zm0-13.244v-3.784h3.784a3.784 3.784 0 0 1-3.784 3.784zM18.92 81.028c-2.613 0-4.73-2.542-4.73-5.676s2.118-5.676 4.73-5.676 4.73 2.541 4.73 5.676-2.119 5.676-4.73 5.676zM35 83.866h-3.782A3.784 3.784 0 0 1 35 80.082zm0-13.244a3.784 3.784 0 0 1-3.784-3.784H35z"  transform="translate(0 -64)"/>
            </svg>
          </span>
          <span className={styles.navigationTitle}>اطلاعات حساب</span>
        </li>
        <li className= {`${step === 4 ? `${styles.activeStep}` : `${styles.activeNo}`}
                        ${step < 5 ? `${styles.activeNo}` : `${styles.activeGreen}`}`}>
          <span className={styles.navIcons}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.181 31.063">
              <path id="shopping-bag" d="M21.356 9.707V7.766a7.766 7.766 0 1 0-15.532 0v1.941H0v16.5a4.854 4.854 0 0 0 4.854 4.854h17.473a4.854 4.854 0 0 0 4.853-4.851V9.707zM9.707 7.766a3.883 3.883 0 1 1 7.766 0v1.941H9.707zm9.707 7.28a1.456 1.456 0 1 1 1.456-1.456 1.456 1.456 0 0 1-1.455 1.456zm-11.649 0a1.456 1.456 0 1 1 1.457-1.456 1.456 1.456 0 0 1-1.456 1.456z"/>
            </svg>
          </span>
          <span className={styles.navigationTitle}>اطلاعات خرید</span>
        </li>
        <li className= {`${step === 5 ? `${styles.activeStep}` : `${styles.activeNo}`}
                        ${step < 5 ? `${styles.activeNo}` : `${styles.activeGreen}`}`}>
          <span className={styles.navIcons}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30.635 30.635">
              <path id="check-circle" d="M38.635 23.317A15.317 15.317 0 1 1 23.317 8a15.317 15.317 0 0 1 15.318 15.317zm-17.089 8.11L32.91 20.063a.988.988 0 0 0 0-1.4l-1.4-1.4a.988.988 0 0 0-1.4 0l-9.268 9.268-4.322-4.322a.988.988 0 0 0-1.4 0l-1.4 1.4a.988.988 0 0 0 0 1.4l6.423 6.423a.988.988 0 0 0 1.4 0z" transform="translate(-8 -8)"/>
            </svg>
          </span>
          <span className={styles.navigationTitle}>  ثبت درخواست</span>
        </li>

      </ul>
    );
  }
}

export default Navigation;
