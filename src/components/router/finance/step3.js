import React, { Component } from "react";
import styles from "../../../styles/finance.scss";

class Step3 extends Component {
    state={
      errors:{}
    }
    validateForm = () => {
      let {values} = this.props;
      let errors = {};
      let formIsValid = true;
      if(!values.bankAccount.match(/^[0-9]+$/)){
        formIsValid = false;
        errors["bankAccount"] = "شماره حساب باید اعداد انگلیسی باد و پر کردن این فیلد ضروری است";
      }
      if (!values.bank) {
        formIsValid = false;
        errors["bank"] = "نام بانک نمی تواند خالی باشد";
      }
      if (!values.bankAccountCreationDate.match(/^[0-9]+$/)) {
        formIsValid = false;
        errors["bankAccountCreationDate"] = "سال افتتاح حساب باد اعداد انگلیسی باشد و پر کردن این فیلد ضروری است ";
      }
      if (!values.bankBranch) {
        formIsValid = false;
        errors["bankBranch"] = "نام شعبه نمی تواند خالی باشد";
      }

      this.setState({
       errors
     });
     return formIsValid;
    }

    continue2 = (e) => {
      e.preventDefault();
      if(this.validateForm()){
        this.props.continue();
      }
    };
  render() {
    const {values , handleChange} = this.props;
    const {errors}= this.state;
    console.log(this.state.errors)
    return (
      <div className={styles.pageContainer}>
        <div className={styles.contentContainer}>
          <div className={`${styles.body} ${styles.inputSameStyles}`}>
            <h2>
              اطلاعات حساب
              <span className={styles.subtitle}>
                همانطور که در شرایط و قوانین مطالعه کردید در این بخش باید اطلاعات
                 <span>صاحب چک</span>
                را به صورت دقیق وارد کنید
              </span>
            </h2>
            <div className={styles.formRow}>
              <div className={styles.formColumnOne}>
                <label>شماره حساب جاری</label>
                <input
                  placeholder="شماره حساب جاری"
                  type="text"
                  onChange={handleChange('bankAccount')}
                  defaultValue={values.bankAccount}
                />
                <div className={styles.errors}>{errors.bankAccount}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>نام بانک</label>
                <input
                  placeholder="نام بانک"
                  type="text"
                  onChange={handleChange('bank')}
                  defaultValue={values.bank}
                />
                <div className={styles.errors}>{errors.bank}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>سال افتتاح حساب </label>
                <input
                  placeholder="سال افتتاح حساب "
                  type="text"
                  onChange={handleChange('bankAccountCreationDate')}
                  defaultValue={values.bankAccountCreationDate}
                />
                <div className={styles.errors}>{errors.bankAccountCreationDate}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>نام و کد شعبه</label>
                <input
                  placeholder="نام و کد شعبه"
                  type="text"
                  onChange={handleChange('bankBranch')}
                  defaultValue={values.bankBranch}
                />
                <div className={styles.errors}>{errors.bankBranch}</div>
              </div>
            </div>
          </div>
          <div className={styles.footer}>
            <button onClick={this.continue2} className={styles.nextStepBtn}>
              مرحله بعد
            </button>
            <button onClick={this.props.back} className={styles.prevStepBtn}>
              بازگشت به مرحله قبل
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Step3;
