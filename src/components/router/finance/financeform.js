import React, { Component } from "react";

import Step1 from "./step1";
import Step2 from "./step2";
import Step3 from "./step3";
import Step4 from "./step4";
import Step5 from "./step5";

class FinanceForm extends Component {
  state = {
    firstName: "",
    lastName: "",
    fatherName: "",
    birthday: "",
    marriageStatus: "",
    nationalCode: "",
    idNumber: "",
    mobile: "",
    address: "",
    addressHome:"",
    phone: "",
    email: "",
    company: "",
    jobTitle: "",
    jobAddress: "",
    addressWork:"",
    jobPhone: "",
    bankAccount: "",
    bank: "",
    bankAccountCreationDate: "",
    bankBranch: "",
    products: "",
    total: "",
    prepayment: "",
    checksNumber: "",
    selectedCity: { value: 'city', label: 'شهر' },
    selectProvince: { value: 'province', label: 'استان' },
    selectedCityWork: { value: 'city', label: 'شهر' },
    selectProvinceWork: { value: '', label: 'استان'},
    selectBirthDay: { value: '', label: 'روز'},
    selectBirthMonth: { value: '', label: 'ماه'},
    selectBirthYear: { value: '', label: 'سال '},
    errors : '',
  };

// handle special step
  continue = () => {
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  handlestepTwo = e => {
    e.preventDefault();
    this.props.stepTwo()
  }
  handlestepThree = e => {
    e.preventDefault();
    this.props.stepThree()
  }
  handlestepFour = e => {
    e.preventDefault();
    this.props.stepFour()
  }

  // handel Feild change
  handleCity = (selectedCity) => {
    this.setState({ selectedCity : selectedCity});
  }
  handleProvince = (selectProvince) => {
      this.setState({ selectProvince: selectProvince});
  }
  handleCityWork = (selectedCityWork) => {
    this.setState({ selectedCityWork : selectedCityWork });
  }
  handleProvinceWork = (selectProvinceWork) => {
    this.setState({ selectProvinceWork : selectProvinceWork });
  }
  handleDay = (selectBirthDay) => {
    this.setState({ selectBirthDay : selectBirthDay });
  }
  handleMonth = (selectBirthMonth) => {
    this.setState({ selectBirthMonth : selectBirthMonth });
  }
  handleYear = (selectBirthYear) => {
    this.setState({ selectBirthYear : selectBirthYear });
  }
  // updateNumber = input => event =>  {
  //   const {errors} = this.state;
  //   if (/^\d+$/.test(event.target.value)) {
  //    this.setState({
  //      [input] : event.target.value
  //    });
  //  }else{
  //   this.setState({errors:"put number"})
  //  }
  // }
  handleChange = input => event => {
   let fullAddress = this.state.selectedCity.value+ ' - ' + this.state.selectProvince.value+ ' - ' +this.state.addressHome;
   let fullAddressJob = this.state.selectedCityWork.value + ' - ' + this.state.selectProvinceWork.value+ ' - ' +this.state.addressWork;
   this.setState({ address : fullAddress })
   this.setState({ jobAddress : fullAddressJob })
   this.setState({ [input] : event.target.value })
   console.log(event.target.value)
 }
  handleRadioButton = (event) => {
    this.setState({
      marriageStatus: event.target.value
    });
  }
  render() {
    const { step} = this.props;
    const {selectBirthDay,selectBirthMonth,selectBirthYear,selectedCity,selectProvince,selectedCityWork,selectProvinceWork,firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,addressHome,phone,email,company,jobTitle,
      jobAddress,addressWork,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber} = this.state;
    const values = {selectBirthDay,selectBirthMonth,selectBirthYear,selectedCity,selectProvince,selectedCityWork,selectProvinceWork,firstName,lastName,fatherName,birthday,marriageStatus,nationalCode,idNumber,mobile,address,addressHome,phone,email,company,jobTitle,
      jobAddress,addressWork,jobPhone,bankAccount,bank,bankAccountCreationDate,bankBranch,products,total,prepayment,checksNumber}
    switch (step) {
      case 1:
        return (
          <Step1
            continue={this.continue}
            handleChange={this.handleChange}
          />
        );
      case 2:
        return (
          <Step2
            continue={this.continue}
            back={this.back}
            handleChange={this.handleChange}
            values = {values}
            handleCity={this.handleCity}
            handleProvince={this.handleProvince}
            handleCityWork={this.handleCityWork}
            handleProvinceWork={this.handleProvinceWork}
            handleRadioButton={this.handleRadioButton}
            handleDay={this.handleDay}
            handleMonth={this.handleMonth}
            handleYear={this.handleYear}
            updateNumber={this.updateNumber}
          />
        );
      case 3:
        return (
          <Step3
            continue={this.continue}
            handleChange={this.handleChange}
            back={this.back}
            values = {values}
          />
        );
      case 4:
        return (
          <Step4
            continue={this.continue}
            handleChange={this.handleChange}
            formData={this.state}
            back={this.back}
            values = {values}
          />
        );
      case 5:
        return (
          <Step5
            continue={this.continue}
            handleChange={this.handleChange}
            values = {values}
            goToStep2={this.goToStep2}
            handlestepTwo = {this.handlestepTwo}
            handlestepThree = {this.handlestepThree}
            handlestepFour = {this.handlestepFour}
          />
        );
      default:
        return <h1>STEP 3</h1>;
    }
  }
}
export default FinanceForm;
