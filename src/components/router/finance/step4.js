import React, { Component } from "react";
import styles from "../../../styles/finance.scss";

class Step4 extends Component {
  state={
    errors:{}
  }
  validateForm = () => {
    let {values} = this.props;
    let errors = {};
    let formIsValid = true;
    if (!values.products) {
      formIsValid = false;
      errors["products"] = "نام محصول نمی تواند خالی باشد";
    }
    if(!values.total.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["total"] = "جمع فاکتور باید اعداد انگلیسی باشد و پر کردن این فیلد ضروری است";
    }

    if (!values.prepayment.match(/^[0-9]+$/)) {
      formIsValid = false;
      errors["prepayment"] = "مبلغ پیش پرداخت باید اعداد انگلیسی باشد و پر کردن این فیلد ضروری است";
    }
    if(!values.checksNumber.match(/^[0-9]+$/)){
      formIsValid = false;
      errors["checksNumber"] = "تعداد اقساط باید اعداد انگلیسی باشد و پر کردن این فیلد ضروری است";
    }
    this.setState({
     errors
   });
   return formIsValid;
  }

  continue2 = (e) => {
    e.preventDefault();
    if(this.validateForm()){
      this.props.continue();
    }
  };
  render() {
    const {values , handleChange} = this.props;
    const{errors}= this.state;
    return (
      <div className={styles.pageContainer}>
        <div className={styles.contentContainer}>
          <div className={`${styles.body} ${styles.inputSameStyles}`}>
            <h2>
              اطلاعات خرید
              <span className={styles.subtitle}>
                اطلاعات خریدتان را به درستی وارد کنید. با این کار رسیدگی به روند بررسی درخواست شما سریع تر انجا خواهد شد
              </span>
            </h2>
            <div className={styles.formRow}>
              <div className={styles.formColumnOne}>
                <label>نام محصول یا محصولاتی که قصد خرید آن را دارید به فارسی تایپ کنید</label>
                <input
                  placeholder="نام محصول"
                  type="text"
                  onChange={handleChange('products')}
                  defaultValue={values.products}
                />
                <div className={styles.errors}>{errors.products}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>جمع مبلغ فاکتور به تومان</label>
                <input
                  placeholder="جمع فاکتور"
                  type="text"
                  onChange={handleChange('total')}
                  defaultValue={values.total}
                />
                <div className={styles.errors}>{errors.total}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>مبلغ پیش پرداخت</label>
                <input
                  placeholder="پیش پرداخت"
                  type="text"
                  onChange={handleChange('prepayment')}
                  defaultValue={values.prepayment}
                />
                <div className={styles.errors}>{errors.prepayment}</div>
              </div>
              <div className={styles.formColumnOne}>
                <label>تعداد اقساط</label>
                <input
                  placeholder="تعداد اقساط"
                  type="text"
                  onChange={handleChange('checksNumber')}
                  defaultValue={values.checksNumber}
                />
                <div className={styles.errors}>{errors.checksNumber}</div>
              </div>
            </div>
          </div>
          <div className={styles.footer}>
            <button onClick={this.continue2} className={styles.nextStepBtn}>
              مرحله بعد
            </button>
            <button onClick={this.props.back} className={styles.prevStepBtn}>
              بازگشت به مرحله قبل
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Step4;
