import React, { Component } from 'react';
import { BrowserRouter , Route  } from 'react-router-dom';

import Home from './components/router/home';
import Status from './components/router/status';
import Finance from './components/router/finance';
import styles from './styles/app.scss'

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <header>
          <div className={styles.Container}>
            <div className={styles.Row}>
              <div className={styles.fullRow}>
                <div className={styles.logo}>
                  <a href="http://Tehranmelody.com">
                    <img alt="Tehran Melody" src={require('./Images/Logo/Logo.png')}/>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className={styles.Container}>
          <div className={styles.Row}>
            <div className={styles.fullRow}>
              <BrowserRouter>
                <div className={styles.mainHome}>
                  <Route exact path="/" component={Home} />
                  <Route path="/status" component={Status} />
                  <Route path="/request" component={Finance} />
                </div>
              </BrowserRouter>
            </div>
          </div>
        </div>
        <footer>
          <div className={styles.Container}>
            <div className={styles.Row}>
              <div className={styles.fullRow}>
                <div className={styles.copyRight}>
                  <a href="http://Tehranmelody.com">Copyright © 2015-2018 Tehranmelody.com</a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default App;
